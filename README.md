# Truco

**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2017<br />

### Objetivo
Implementação do jogo de Truco com o uso de:
    - Multithread 
    - Sockets
    
### Observação

IDE:  [IntelliJ IDEA](https://www.jetbrains.com/idea/)<br />
Linguagem: [JAVA](https://www.java.com/)<br />
Banco de dados: Não utiliza<br />
São necessarios quatro clientes conectarem ao servidor para jogar
Para alterar o ip do host, altere a variavel "IPServidor" no cliente 

### Execução

    $ cd src/
    $ javac -g -cp gson-2.8.0.jar *.java
    
#### Servidor

    $ java -cp gson-2.8.0.jar;. ServidorTCP
    
#### Cliente

    $ java -cp gson-2.8.0.jar;. ClienteTCP
    
### Pacotes

| Nome | Função |
| ------ | ------ |
| [Gson] | Framework base |

[Gson]: <https://github.com/google/gson>

### Contribuição

Esse projeto está concluído e é livre para uso.

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->

