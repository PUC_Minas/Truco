import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.net.ServerSocket;
import java.io.*;

class Carta{
	String nipe;
	String valor;
	
	public Carta( String nipe, String valor){
		this.nipe = nipe;
		this.valor = valor;
	}
	
	public String getNipe( ){
		return nipe;
	}
	
	
	public String getValor( ){
		return valor;
	}
	
	public void setValor( String nipe ){
		this.nipe = nipe;
	}
	
	public void setNipe( String valor ){
		this.valor = valor;
	}
}

class Baralho{
	
	private static List<Carta> baralho = new ArrayList<Carta>( );  
	
	private static void montarBaralho( ){
		baralho.add(new Carta("Ouro", "A"));
		baralho.add(new Carta("Ouro", "2"));
		baralho.add(new Carta("Ouro", "3"));
		baralho.add(new Carta("Ouro", "4"));
		baralho.add(new Carta("Ouro", "5"));
		baralho.add(new Carta("Ouro", "6"));
		baralho.add(new Carta("Ouro", "7"));
		baralho.add(new Carta("Ouro", "J"));
		baralho.add(new Carta("Ouro", "Q"));
		baralho.add(new Carta("Ouro", "K"));
		
		baralho.add(new Carta("Copas", "A"));
		baralho.add(new Carta("Copas", "2"));
		baralho.add(new Carta("Copas", "3"));
		baralho.add(new Carta("Copas", "4"));
		baralho.add(new Carta("Copas", "5"));
		baralho.add(new Carta("Copas", "6"));
		baralho.add(new Carta("Copas", "7"));
		baralho.add(new Carta("Copas", "J"));
		baralho.add(new Carta("Copas", "Q"));
		baralho.add(new Carta("Copas", "K"));
		
		baralho.add(new Carta("Paus", "A"));
		baralho.add(new Carta("Paus", "2"));
		baralho.add(new Carta("Paus", "3"));
		baralho.add(new Carta("Paus", "4"));
		baralho.add(new Carta("Paus", "5"));
		baralho.add(new Carta("Paus", "6"));
		baralho.add(new Carta("Paus", "7"));
		baralho.add(new Carta("Paus", "J"));
		baralho.add(new Carta("Paus", "Q"));
		baralho.add(new Carta("Paus", "K"));
		
		baralho.add(new Carta("Espada", "A"));
		baralho.add(new Carta("Espada", "2"));
		baralho.add(new Carta("Espada", "3"));
		baralho.add(new Carta("Espada", "4"));
		baralho.add(new Carta("Espada", "5"));
		baralho.add(new Carta("Espada", "6"));
		baralho.add(new Carta("Espada", "7"));
		baralho.add(new Carta("Espada", "J"));
		baralho.add(new Carta("Espada", "Q"));
		baralho.add(new Carta("Espada", "K"));
	}
	
	public static void embaralhar( ){
		if(baralho.size() != 40){
			montarBaralho();
		}
		Collections.shuffle(baralho);		
	}
	
	public static List<Carta> sacarCartas( ){ 
		List<Carta> maoDoJogador = new ArrayList<Carta>( );
		for(int i =0; i<4; i++){
			maoDoJogador.add(baralho.get(i));
			baralho.remove(i);
		}
		return maoDoJogador;
	}
}

class Jogador{
	String nome;
	Socket socket;
	String time;
	
	public Jogador(String nome, Socket socket){
		this.nome = nome;
		this.socket = socket;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	
}

public class ServidorTCP{
	
	private static List<Jogador> jogadores;
	
	//M�TODO PRINCIPAL DA CLASSE
	public static void main (String args[])	{
		jogadores = new ArrayList<Jogador>( );
		try{
			int PortaServidor = 7000;
			
			//INICIALIZA UM SERVI�O DE ESCUTA POR CONEX�ES NA PORTA ESPECIFICADA
			System.out.println(" -S- Aguardando conexao (P:"+PortaServidor+")...");
			ServerSocket socktServ = new ServerSocket(PortaServidor);
			
			for(int i = 0; i<4; i++){
				//ESPERA (BLOQUEADO) POR CONEX�ES			
				Socket conSer = socktServ.accept(); //RECEBE CONEX�O E CRIA UM NOVO CANAL (p) NO SENTIDO CONTR�RIO (SERVIDOR -> CLIENTE)
				System.out.println(" -S- Conectado ao cliente ->" + conSer.toString());
				//CRIA UM PACOTE DE ENTRADA PARA RECEBER MENSAGENS, ASSOCIADO � CONEX�O (p)
				ObjectInputStream sServIn = new ObjectInputStream(conSer.getInputStream());
				Object msgIn = sServIn.readObject(); //ESPERA (BLOQUEADO) POR UM PACOTE
				jogadores.add(new Jogador(msgIn.toString(), conSer));
			}
			
			
			//CRIA UM PACOTE DE SA�DA PARA ENVIAR MENSAGENS, ASSOCIANDO-O � CONEX�O (p)
			ObjectOutputStream sSerOut = new ObjectOutputStream(conSer.getOutputStream());
			sSerOut.writeObject("RETORNO " + msgIn.toString() + " - TCP"); //ESCREVE NO PACOTE
			System.out.println(" -S- Enviando mensagem resposta...");
			sSerOut.flush(); //ENVIA O PACOTE

			//FINALIZA A CONEX�O
			socktServ.close();
			conSer.close();
			System.out.println(" -S- Conexao finalizada...");

		}
		catch(Exception e) //SE OCORRER ALGUMA EXCESS�O, ENT�O DEVE SER TRATADA (AMIGAVELMENTE)
		{
			System.out.println(" -S- O seguinte problema ocorreu : \n" + e.toString());
		}
	}
}